package automindtest.RunnerGeral;

import automindtest.utils.ReportWeb;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(Cucumber.class)
@CucumberOptions(
	features = {"src/main/java/automindtest/projeto"},
	glue = {"automindtest.projeto","automindtest.utils"},
//	monochrome=true,
//	plugin = {"pretty", "html:target/report-html"},
	plugin = {"pretty", "json:target/report-project-test/cucumber.json"},
//	snippets = SnippetType.UNDERSCORE,
//	strict = false
	tags = "@Regressivo"
)

public class RunnerGeralTest {
	@AfterClass
	public static void gerarReport(){
		try {
			ReportWeb.gerarReport("API e Web");
			Runtime.getRuntime().exec("cmd.exe /c mvn cluecumber-report:reporting");

		}catch (IOException e){
			e.printStackTrace();
		}
	}
}


