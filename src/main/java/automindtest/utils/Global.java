package automindtest.utils;

import automindtest.Support.SetUp;
import automindtest.printsc.ScenarioRep;
import com.itextpdf.text.Image;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.io.FileUtils;
import org.easetech.easytest.util.TestInfo;
import org.junit.Assert;
import org.junit.Rule;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static automindtest.utils.Helper.getDateHour;
import static org.junit.Assert.assertTrue;

public class Global extends SetUp {


    public String path = "target//report-html//";
    public static Document document = new Document();
//	Actions action = new Actions(get_driver());

    public Global() {
//		Actions action = new Actions(get_driver());
    }

    public enum Identify {
        ID, NAME, XPATH, LINKTEXT, CSS, CLASSNAME;
    }

    public enum Technology {
        SELENIUM_WEB, APPIUM, API;
    }

    public enum TipoExec {
        LOCAL, GRID
    }


    public void fillField(Identify by, String identify_value, String value) {

        WebElement webElement = getElementApplication(by, identify_value);
        webElement.sendKeys(value);
    }

    public void clickButton(Identify by, String identify_value) {

        WebElement webElement = getElementApplication(by, identify_value);
        webElement.click();
    }

    public void selectComboBox(Identify by, String identify_value, String value) {

        WebElement comboBox = getElementApplication(by, identify_value);
        Select combo = new Select(comboBox);
        combo.selectByVisibleText(value);
    }

    public boolean waitElementVisible(Identify by, String identify_value, int seconds) {

        WebDriverWait wait = new WebDriverWait(SetUp.get_driver(), seconds);

        try {

            switch (by) {

                case ID:
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(identify_value)));
                    break;

                case NAME:
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(identify_value)));
                    break;

                case XPATH:
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(identify_value)));
                    break;

                case LINKTEXT:
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(identify_value)));
                    break;

                case CSS:
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(identify_value)));
                    break;

                case CLASSNAME:
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(identify_value)));
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean elementExist(Identify by, String identify_value, int seconds) {

        List<WebElement> elementos = null;

        for (int i = 0; i < seconds; i++) {

            switch (by) {

                case ID:
                    elementos = SetUp.get_driver().findElements(By.id(identify_value));
                    break;

                case NAME:
                    elementos = SetUp.get_driver().findElements(By.name(identify_value));
                    break;

                case XPATH:
                    elementos = SetUp.get_driver().findElements(By.xpath(identify_value));
                    break;

                case LINKTEXT:
                    elementos = SetUp.get_driver().findElements(By.linkText(identify_value));
                    break;

                case CSS:
                    elementos = SetUp.get_driver().findElements(By.cssSelector(identify_value));
                    break;

                case CLASSNAME:
                    elementos = SetUp.get_driver().findElements(By.className(identify_value));
                    break;

                default:
                    break;
            }

            if (elementos.size() > 0)
                return true;

            wait(1);
        }

        return false;
    }

    public void wait(int seconds) {

        try {
            Thread.sleep(seconds * 1000);
        } catch (Exception e) {
        }
    }

    public WebElement getElementApplication(Identify by, String identify_value) {
        WebElement webElement = null;

        try {

            switch (by) {

                case ID:
                    webElement = SetUp.get_driver().findElement(By.id(identify_value));
                    break;

                case NAME:
                    webElement = SetUp.get_driver().findElement(By.name(identify_value));
                    break;

                case XPATH:
                    webElement = SetUp.get_driver().findElement(By.xpath(identify_value));
                    break;

                case LINKTEXT:
                    webElement = SetUp.get_driver().findElement(By.linkText(identify_value));
                    break;

                case CSS:
                    webElement = SetUp.get_driver().findElement(By.cssSelector(identify_value));
                    break;

                case CLASSNAME:
                    webElement = SetUp.get_driver().findElement(By.className(identify_value));
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            Assert.fail("Elemento: '" + identify_value + "' NÃO encontrado.");
        }

        return webElement;
    }

    public boolean checkElement(String xpath) {
        try {
            return SetUp.get_driver().findElement(By.xpath(xpath)).isDisplayed();

        } catch (Exception e) {
            return false;
        }
    }

    public boolean waitElement(String xpath) {

        WebDriverWait aguardar = new WebDriverWait(SetUp.get_driver(), 10);
        return aguardar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath))).isDisplayed();
//		return aguardar.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath))).isDisplayed();					
    }


    public void PageUP(String textLink) {
        try {
            Actions action = new Actions(SetUp.get_driver());
            action.moveToElement(SetUp.get_driver().findElement(By.xpath(textLink)));
            action.sendKeys(Keys.PAGE_UP).build().perform();
        } catch (Exception e) {
            assertTrue("Não encontrou Xpath (pageup) no Link ' " + textLink.toString() + " '", false);
        }

    }

    public void movePageUpDown(WebElement element, String UpDown) {
        try {
            if (UpDown.equalsIgnoreCase("UP")) {
                Actions action = new Actions(SetUp.get_driver());
                action.moveToElement(element);
                action.sendKeys(Keys.PAGE_UP).build().perform();
            } else if (UpDown.equalsIgnoreCase("Down")) {
                Actions action = new Actions(SetUp.get_driver());
                action.moveToElement(element);
                action.sendKeys(Keys.PAGE_DOWN).build().perform();

            }
        } catch (Exception e) {
            assertTrue("Não encontrou o Elemento: " + element.toString() + " para fazer Page_" + UpDown, false);
        }

    }


    public void moveMouseToElement(WebElement element) {
        Actions action = new Actions(SetUp.get_driver());
        try {
            action.moveToElement(element).build().perform();
        } catch (Exception e) {
            assertTrue("Nao foi possivel mover o mouse para o elemento: " + element.toString() + " '", false);
        }

    }


    public Object execJS(String cmd, Object... param) {
        JavascriptExecutor js = (JavascriptExecutor) SetUp.get_driver();
        return js.executeScript(cmd, param);
    }

    public void scrollToElementDownUp(WebElement element, String downUp) {
        try {
            if (downUp.equalsIgnoreCase("Down")) {
                for (int i = 0; i < 6; i++) {
                    if (!element.isDisplayed()) {
                        execJS("window.scrollBy(0, arguments[0]", element.getLocation().y + i);
                    } else {
                        break;
                    }

                }

            } else if (downUp.equalsIgnoreCase("Up")) {
                for (int i = 0; i < 6; i++) {
                    if (!element.isDisplayed()) {
                        execJS("window.scrollTo(0, -i);");
                    } else {
                        break;
                    }

                }
            }

        } catch (Exception e) {
            System.out.println("Elemento não encontrado para mover barra de scroll: " + e.getMessage());
        }
    }

    public void screanshot(String path) {

        File scrFile = ((TakesScreenshot) SetUp.get_driver()).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File(path + "\\" + getDateHour() + ".png"));
        } catch (Exception e) {
            Assert.fail("Erro ao gerar o Screenshot '" + path + "'.");
        }
    }

    @Rule
    public File evidencia() throws IOException {
        TestInfo testName = new TestInfo(null);
        TakesScreenshot ss = (TakesScreenshot) SetUp.get_driver();
        File arquivo = ss.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(arquivo, new File(path + File.separator + testName.getTestClass() + "_" + getDateHour() + ".png"));
        return arquivo;
    }

    public void GeraPDF(String TituloReport) {
        // criação do objeto documento


        try {

            PdfWriter.getInstance(document, new FileOutputStream("target//report//ReportTest_" + getDateHour() + ".pdf"));
            document.open();

            // adicionando um parágrafo ao documento
            document.add(new Paragraph(TituloReport));

            // adicionando um parágrafo com fonte diferente ao arquivo
            document.add(new Paragraph("#### Report Test ####", FontFactory.getFont(FontFactory.COURIER, 12)));
            Image image = Image.getInstance(evidencia().getAbsolutePath());
//            image.setAbsolutePosition(40, 50);
            image.scaleAbsolute(530f, 300f);
            document.add(image);

            document.newPage();

        } catch (DocumentException de) {
            System.err.println(de.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        } finally {
        }
    }

    public void colarTexto(WebElement elemetoWeb, String textoParaColar) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        try {

            StringSelection strSelection = new StringSelection(textoParaColar);
            clipboard.setContents(strSelection, null);
            elemetoWeb.sendKeys(Keys.CONTROL + "v");

        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {

        }
    }

    public void loginSikuli(String user, String password) {

        Screen screen = new Screen();
        try {
            screen.type(user);
            screen.type(Key.TAB);
            screen.type(password);
            screen.type(Key.TAB);
            screen.type(Key.ENTER);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    public void enviaTextoSikuli(String Texto) {
        Screen screen = new Screen();
        try {
            screen.type(Texto);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void enviaTabSikuli() {
        Screen screen = new Screen();
        try {
            screen.type(Key.TAB);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void enviaEnterSikuli() {
        Screen screen = new Screen();
        try {
            screen.type(Key.ENTER);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static Properties getPropriedades() throws IOException {
        try {
            Properties props = new Properties();
            FileInputStream file = new FileInputStream("src" + File.separator +
                    "test" + File.separator +
                    "resources" + File.separator +
                    "start.properties");
            props.load(file);
            return props;
        } catch (Exception e) {
            System.err.println("Ocorreu erro ao carregar arquivo start.properties " + e.getMessage());
            return null;
        }
    }

    public void logPrint(String message, WebElement webElement, boolean print) {
        try {
            wait.until(ExpectedConditions.visibilityOf(webElement));
            if (message != null)
                ScenarioRep.insereMensagem(message);
            if (print)
                ScenarioRep.printScreen(get_driver(), webElement);

        } catch (ElementNotInteractableException e) {
            ScenarioRep.insereMensagem("Falhou ao interagir com o elemento: " +webElement);
        } catch (Exception e) {
            ScenarioRep.insereMensagem("Falhou : " +e.getMessage());
        }
    }

    public void logPrint(String message, boolean print) {
        try {
            if (message != null)
                ScenarioRep.insereMensagem(message);
            if (print)
                ScenarioRep.printScreen(get_driver());

        } catch (ElementNotInteractableException e) {
            ScenarioRep.insereMensagem("Falhou ao tirar print!! ");
        } catch (Exception e) {
            ScenarioRep.insereMensagem("Falhou : " +e.getMessage());
        }
    }


}
