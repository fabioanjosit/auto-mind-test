package automindtest.utils;

import com.rajatthareja.reportbuilder.Color;
import com.rajatthareja.reportbuilder.ReportBuilder;
import org.apache.xmlgraphics.image.loader.impl.imageio.PreloaderImageIO;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportWeb {

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static DateFormat dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
    private static Date date = new Date();

    public static void gerarReport(String tituloTest){

        ReportBuilder reportBuilder = new ReportBuilder();
        reportBuilder.setReportDirectory("target/report-project-test/");

        reportBuilder.setReportFileName(dateFormatFile.format(date));

        reportBuilder.setReportTitle("AutoMindTest - "+tituloTest );
        reportBuilder.setReportColor(Color.INDIGO);

        reportBuilder.setAdditionalInfo("Date: ", dateFormat.format(date));
        reportBuilder.setAdditionalInfo("Ambiente: ", "Homolog");
        reportBuilder.setAdditionalInfo("Teste: ", tituloTest);

        List<Object> cucumberJsonReports = new ArrayList<>();
        cucumberJsonReports.add(new File("target/report-project-test/cucumber.json"));

        reportBuilder.build(cucumberJsonReports);


    }
}
