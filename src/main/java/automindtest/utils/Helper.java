package automindtest.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import automindtest.Support.SetUp;

public class Helper extends SetUp{

//	public Helper(WebDriver driver) {
//		super(driver);
//	}
	
	public Helper() {
		// TODO Auto-generated constructor stub
	}

	public static String readFileTxt(String pathFile) {

		String value = "";
		try {
			BufferedReader file = new BufferedReader(new FileReader(pathFile));

			String line;

			while ((line = file.readLine()) != null) {
				value = value + line;
			}
			file.close();

		} catch (Exception e) {
			Assert.fail("Erro ao utilizar o arquivo '" + pathFile + "'.");
		}

		return value;
	}

	public static String getDateHour() {
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");

		return dateFormat.format(date);
	}
	
	public static String removerCaracteresEspeciais(String string) {
		String normalizada="";
		string = Normalizer.normalize(string, Normalizer.Form.NFD);
        string = string.replaceAll("[^\\p{ASCII}]", "");
        string = string.replace("-", "");
        string = string.replace(".", "");
        string = string.replace("'", "");
        string = string.replace("´", "");
        string = string.replace("`", "");
        string = string.replace("ç", "c");
        for(int x=0; x < string.length(); x++){
    		switch (string.charAt(x)) {
    		case 'á':
    		case 'â':
    		case 'à':
    		case 'ã':
    		case 'ä':
    				normalizada+="a";				
    			break;
    		
    		case 'é':
    		case 'ê':
    		case 'è':
    		case 'ë':
    				normalizada+="e";				
    			break;

    		case 'í':
    		case 'î':
    		case 'ì':
    		case 'ĩ':
    		case 'ï':
    				normalizada+="i";				
    			break;
    		case 'ó':
    		case 'ô':
    		case 'ò':
    		case 'õ':
    		case 'ö':
    				normalizada+="o";				
    			break;
    			
    		case 'ú':
    		case 'û':
    		case 'ù':
    		case 'ũ':
    		case 'ü':
    				normalizada+="u";				
    			break;
    			
    		default:
    				normalizada+=string.charAt(x);
    			break;
    		}
        }
        
        return normalizada;
    }
	
}
