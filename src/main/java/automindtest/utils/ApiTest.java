package automindtest.utils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import automindtest.printsc.ScenarioRep;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonElement;

import automindtest.Support.SetUp;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ApiTest extends SetUp {


    private Logger logger = Logger.getLogger(ApiTest.class);
    public static Response responsePub;
    public static RequestSpecification httpReq;
    public static String getJson;
    public static String token;
    public static int respStatusCode;
    public static String respStatus;
    public static JsonElement jsonElement;
    public static String jsonValue;


    public ApiTest() {
        this.logger = Logger.getLogger(ApiTest.class);
    }

    public static void setURL(String url) {
        try {
            RestAssured.baseURI = url;
            httpReq = RestAssured.given();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Response reqGet() {

        responsePub = httpReq.get(RestAssured.baseURI);
        respStatusCode = responsePub.getStatusCode();
        getJson = responsePub.getBody().prettyPrint();
        System.out.println(getJson);
        System.out.println(respStatusCode);
        logRequestResponseData("GET",RestAssured.baseURI,"",null);
        return responsePub;
    }

    public static Response reqGet(Map<String, String> header, Map<String, String> param, boolean setup) {
        responsePub = null;
        try {
            if (setup) {
                httpReq
                        .config(RestAssured.config()
                                .encoderConfig(EncoderConfig.encoderConfig()
                                        .encodeContentTypeAs("x-www-form-urlencoded", ContentType.URLENC)));
            }

            if (header != null) {
                Set<String> keys = header.keySet();
                for (String key : keys) {
                    httpReq.header(key, header.get(key));
                }
            }

            if (param != null) {
                Set<String> keys = param.keySet();
                for (String key : keys) {
                    httpReq.formParam(key, param.get(key));
                }
            }

            httpReq.when().relaxedHTTPSValidation();

            responsePub = httpReq.get(RestAssured.baseURI);
            respStatusCode = responsePub.getStatusCode();
            getJson = responsePub.getBody().prettyPrint();
            System.out.println(getJson);
            System.out.println(respStatusCode);
            logRequestResponseData("GET",RestAssured.baseURI,header.toString(),null);
            return responsePub;
        } catch (Exception e) {
            e.printStackTrace();
            return responsePub;
        }
    }

    public static Response reqPost(InputStream jsonPath, Map<String, String> header, Map<String, String> param, boolean setup) {
        InputStream jsonFile;
        String json = null;
        responsePub = null;
        httpReq = RestAssured.given().relaxedHTTPSValidation();

        try {
            if (setup) {
                httpReq
                        .config(RestAssured.config()
                                .encoderConfig(EncoderConfig.encoderConfig()
                                        .encodeContentTypeAs("raw", ContentType.JSON)));
            }

            if (header != null) {
                Set<String> keys = header.keySet();
                for (String key : keys) {
                    httpReq.header(key, header.get(key));
                }
            }

            if (param != null) {
                Set<String> keys = param.keySet();
                for (String key : keys) {
                    httpReq.formParam(key, param.get(key));
                }
            }

            jsonFile = jsonPath;
            json = IOUtils.toString(jsonFile, String.valueOf(StandardCharsets.UTF_8));

            httpReq.body(json);
            httpReq.when();

            responsePub = httpReq.post(RestAssured.baseURI);
            respStatusCode = responsePub.getStatusCode();
            getJson = responsePub.getBody().prettyPrint();
            System.out.println(getJson);
            System.out.println(respStatusCode);
            logRequestResponseData("POST",RestAssured.baseURI,header.toString(),new JSONObject(jsonPath.toString()));
            return responsePub;

        } catch (Exception e) {
            e.printStackTrace();
            return responsePub;
        }
    }


    public static Response reqPost(JSONObject jsonStr, Map<String, String> header, Map<String, String> param, boolean setup) {
        responsePub = null;
        httpReq = RestAssured.given().relaxedHTTPSValidation();

        try {
            if (setup) {
                httpReq
                        .config(RestAssured.config()
                                .encoderConfig(EncoderConfig.encoderConfig()
                                        .encodeContentTypeAs("raw", ContentType.JSON)));
            }

            if (header != null) {
                Set<String> keys = header.keySet();
                for (String key : keys) {
                    httpReq.header(key, header.get(key));
                }
            }

            if (param != null) {
                Set<String> keys = param.keySet();
                for (String key : keys) {
                    httpReq.formParam(key, param.get(key));
                }
            }

            httpReq.body(jsonStr);
            httpReq.when();

            responsePub = httpReq.post(RestAssured.baseURI);
            respStatusCode = responsePub.getStatusCode();
            getJson = responsePub.getBody().prettyPrint();
            System.out.println(RestAssured.baseURI);
            System.out.println(jsonStr);
            System.out.println(getJson);
            System.out.println(respStatusCode);
            logRequestResponseData("POST",RestAssured.baseURI,header.toString(),jsonStr);
            return responsePub;

        } catch (Exception e) {
            e.printStackTrace();
            return responsePub;
        }
    }


    public static Response reqPatch() {

        responsePub = httpReq.patch(RestAssured.baseURI);
        respStatusCode = responsePub.getStatusCode();
        getJson = responsePub.getBody().prettyPrint();
        System.out.println(getJson);
        System.out.println(respStatusCode);
        logRequestResponseData("PATCH",RestAssured.baseURI,"",null);
        return responsePub;
    }

    public static Response reqDelete() {

        responsePub = httpReq.delete(RestAssured.baseURI);
        respStatusCode = responsePub.getStatusCode();
        getJson = responsePub.getBody().prettyPrint();
        System.out.println(getJson);
        System.out.println(respStatusCode);
        logRequestResponseData("DELETE",RestAssured.baseURI,"",null);
        return responsePub;
    }

    public static Response reqPut() {


        responsePub = httpReq.put(RestAssured.baseURI);
        respStatusCode = responsePub.getStatusCode();
        getJson = responsePub.getBody().prettyPrint();
        System.out.println(getJson);
        System.out.println(respStatusCode);
        logRequestResponseData("PUT",RestAssured.baseURI,"",null);
        return responsePub;
    }


    public static Map<String, String> setHeader() {
        Map<String, String> header = new HashMap<String, String>();
        header.put("Accept:", "application/json");

        return header;
    }


    public static JSONObject setValueByKeyJson(String file, Map<String, String> mapFindKeys) {
        JSONObject jsonFile = parseFileJson(file);
        JSONObject jsonFinal = null;

        for (Map.Entry<String, String> entry : mapFindKeys.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            try {
                jsonFinal = updateByKeyJson(jsonFile, key, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonFinal;
    }

    public static JSONObject parseFileJson(String strFile) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(
                    (SetUp.getProp().getProperty("prop.path.resources") + strFile))));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject(content);
    }

    public static JSONObject updateByKeyJson(JSONObject jsonObj, String keyFind, String valueNew) {

        Iterator<?> iteator = jsonObj.keys();
        String key = null;

        while (iteator.hasNext()) {
            key = (String) iteator.next();

            if ((jsonObj.optJSONArray(key) == null) && (jsonObj.optJSONObject(key) == null)) {
                if ((key.equals(keyFind))) {
                    jsonObj.put(key, valueNew);
                }
            }

            if (jsonObj.optJSONObject(key) != null) {
                updateByKeyJson(jsonObj.getJSONObject(key), keyFind, valueNew);
            }

            if (jsonObj.optJSONArray(key) != null) {
                JSONArray jArray = jsonObj.getJSONArray(key);
                for (int i = 0; i < jArray.length(); i++) {
                    updateByKeyJson(jArray.getJSONObject(i), keyFind, valueNew);
                }
            }

        }
        return jsonObj;
    }


    public String getValueByKeyJson(JSONObject jsonObj, String keyFind) {

        Iterator<?> iterator = jsonObj.keys();
        String key = null;
        while (iterator.hasNext()) {
            key = (String) iterator.next();

            if ((jsonObj.optJSONArray(key) == null) && (jsonObj.optJSONObject(key) == null)) {
                if ((key.equals(keyFind))) {
                    jsonValue = jsonObj.getString(key).toString();
                }
            }

            if (jsonObj.optJSONObject(key) != null) {
                getValueByKeyJson(jsonObj.getJSONObject(key), keyFind);
            }

            if (jsonObj.optJSONArray(key) != null) {
                JSONArray jsonArray = jsonObj.getJSONArray(key);
                for (int i = 0; i < jsonArray.length(); i++) {
                    getValueByKeyJson(jsonArray.getJSONObject(i), keyFind);
                }
            }
        }
        return jsonValue;
    }

    public static void logRequestResponseData(String metodo, String url, String headers, JSONObject body) {
        ScenarioRep.insereMensagem(metodo + ": " + url);
        ScenarioRep.insereMensagem("Headers: \n" + headers);
        if (body != null)
            ScenarioRep.insereMensagem("Body: \n" + body);
        ScenarioRep.insereMensagem("=============== Response ================");
        ScenarioRep.insereMensagem("Status Code: " + responsePub.statusCode());
        if (!responsePub.asString().isEmpty() && (responsePub.asPrettyString().contains("{")))
            ScenarioRep.insereMensagem("Response: \n" + responsePub.jsonPath().prettyPrint());
        ScenarioRep.insereMensagem("Tempo consumido: " + responsePub.getTimeIn(TimeUnit.SECONDS) + " seg.");
        ScenarioRep.insereMensagem("__________________________________________");

    }


}
