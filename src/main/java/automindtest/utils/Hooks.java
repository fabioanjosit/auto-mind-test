package automindtest.utils;

import java.io.IOException;
import automindtest.Support.SetUp;
import automindtest.printsc.ScenarioRep;
import io.cucumber.java.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import static automindtest.printsc.ScenarioRep.*;

public class Hooks extends SetUp{
	

	public  boolean  FECHAR_NAVEGADOR    = true; 
	
//	public  Hooks() throws IOException {
//		super(get_driver());
//		// TODO Auto-generated constructor stub
//	}

	@Before
	public void start_test(Scenario scenario) throws IOException {
		SetUp.scenarioTag = scenario.getSourceTagNames().iterator().next();
		if (!scenario.getSourceTagNames().iterator().next().contains("@API")){
			SetUp.get_driver();
		}
		add(scenario);
	}
	
	@After
	public void FecharNavegador(Scenario scenario) { 
		if(Global.document!=null){
			Global.document.close();
		}
		
		if (FECHAR_NAVEGADOR){
			SetUp.tear_down();
		}
		remove();
	}
}
