package automindtest.Support;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import automindtest.printsc.ScenarioRep;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import automindtest.utils.Global.TipoExec;
import automindtest.utils.Hooks;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SetUp {

    static int timeout = 5;
    public static WebDriverWait wait;
    public static String scenarioTag;
    public static WebDriver webDriverFake;

    public SetUp() {
        // TODO Auto-generated constructor stub
    }

    public enum IdentifyNav {
        CHROME, IE, FIREFOX;
    }

    public static TipoExec TIPO_EXEC = TipoExec.LOCAL;

    private static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<WebDriver>() {
        @Override
        protected synchronized WebDriver initialValue() {
            if (!scenarioTag.contains("@API"))
                return initDriver();
            else
                return webDriverFake;
        }
    };

    public static WebDriver get_driver() {
        return threadDriver.get();
    }

    public static WebDriver initDriver() {
        WebDriver driver = null;
        try {
            Properties prop = getProp();
            driver = setUp(driver, IdentifyNav.valueOf(prop.getProperty("prop.website.navegador")), prop.getProperty("prop.website.url"));
            wait = new WebDriverWait(driver, timeout);
            return driver;
        } catch (Exception e) {
            System.err.println("Ocorreu erro ao iniciar get_driver " + e.getMessage());
            return driver;
            // TODO: handle exception
        }
    }

    public static void tear_down() {
        WebDriver driver = get_driver();
        if (driver != null) {
            driver.close();
            driver.quit();
            driver = null;
        }
        if (threadDriver != null) {
            threadDriver.remove();
        }
    }

    public static Properties getProp() throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream("src" + File.separator + "test" + File.separator +
                "resources" + File.separator + "start.properties");
        props.load(file);
        return props;
    }

    public static WebDriver setUp(WebDriver driver, IdentifyNav by, String url) {
        try {
            if ((getProp().getProperty("prop.tipo.exec").equalsIgnoreCase("LOCAL"))) {
                System.out.println("$$$$$$ Automção WEB - Local $$$$$$$");
                switch (by) {
                    case CHROME:
                        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
                        System.out.println("ChromeOptions...");
                        ChromeOptions options = new ChromeOptions();
//						DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                        options.setExperimentalOption("useAutomationExtension", false);
//						options.setExperimentalOption("excludeSwitches",Arrays.asList("disable-popup-blocking"));
                        options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
                        options.addArguments("--disable-infobars");
                        options.addArguments("--disable-notifications");
//						options.addArguments("--no-startup-window");
//						options.addArguments("--headless");
//						capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                        driver = new ChromeDriver(options);
                        break;

                    case IE:
                        System.setProperty("webdriver.ie.driver", "src\\test\\resources\\IEDriverServer.exe");
                        driver = new InternetExplorerDriver();
                        break;

                    case FIREFOX:
                        System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\geckodriver.exe");
                        driver = new FirefoxDriver();
                        break;

                    default:
                        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
                        driver = new ChromeDriver();
                        break;
                }
                // execucao por GRID
            } else if ((Hooks.getProp().getProperty("prop.tipo.exec").equalsIgnoreCase("GRID"))) {
                System.out.println("$$$$$$ Automação WEB - Selenium GRID $$$$$$$");
                DesiredCapabilities cap;
                switch (by) {
                    case CHROME:
                        cap = DesiredCapabilities.chrome();
                        break;

                    case IE:
                        cap = DesiredCapabilities.internetExplorer();
                        break;

                    case FIREFOX:
                        cap = DesiredCapabilities.firefox();
                        break;

                    default:
                        cap = DesiredCapabilities.chrome();
                        break;
                }
                driver = new RemoteWebDriver(new URL(Hooks.getProp().getProperty("prop.url.grid.hub")), cap);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.err.println("Falha ao executar o WEBDRIVER CUSTOM");
            e.printStackTrace();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(url);
        return driver;
    }

    public SetUp(WebDriver driver) {
//		SetUp.driver = driver;
    }


}
