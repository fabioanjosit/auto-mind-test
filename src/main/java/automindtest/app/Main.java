package automindtest.app;

import automindtest.Support.SetUp;
import io.cucumber.core.cli.CommandlineOptions;

import java.util.stream.Stream;

public class Main {

    private static String[] defaultOptions = {
            "classpath:features/",
            CommandlineOptions.MONOCHROME,
            CommandlineOptions.GLUE, "automindtest.projeto.orangehrm.steps",
            CommandlineOptions.GLUE, "automindtest.utils",
            CommandlineOptions.PLUGIN, "pretty",
            CommandlineOptions.PLUGIN, "json:target/cucumber-report/cucumber.json"
    };

    public static void main(String[] args){
        Stream<String> cucumberOptions = Stream.concat(Stream.of(defaultOptions), Stream.of(args));
        int FinalResult = io.cucumber.core.cli.Main.run(cucumberOptions.toArray(String[]::new));

        automindtest.projeto.orangehrm.runner.RunnerTest.gerarReport();
        SetUp.tear_down();

        System.exit(FinalResult);


    }
}
