package automindtest.projeto.orangehrm.runner;

import automindtest.utils.ReportWeb;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

//import cucumber.api.CucumberOptions;

import java.io.IOException;

@RunWith(Cucumber.class)
@CucumberOptions(
	features = {"src/main/java/automindtest/projeto/orangehrm/feature"},
	glue = {"automindtest.projeto.orangehrm.steps","automindtest.utils",
            "automindtest.projeto.orangehrm.page","automindtest.projeto.orangehrm.projeto"},
//	monochrome=true,
//	plugin = {"pretty", "html:target/report-html"},
	plugin = {"pretty", "json:target/cucumber-report/cucumber.json"},
//	snippets = SnippetType.UNDERSCORE,
//	strict = false
	tags = "@Run"
)

public class RunnerTest {
	@AfterClass
	public static void gerarReport(){
		try {
			ReportWeb.gerarReport("Sistema OrangeHRM");
			Runtime.getRuntime().exec("cmd.exe /c mvn cluecumber-report:reporting");

		}catch (IOException e){
			e.printStackTrace();
		}
	}
}


