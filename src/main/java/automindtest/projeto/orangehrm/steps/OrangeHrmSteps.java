package automindtest.projeto.orangehrm.steps;

import automindtest.Support.SetUp;
import automindtest.projeto.orangehrm.page.HomePage;
import automindtest.projeto.orangehrm.page.LoginPage;
import automindtest.utils.Global;
import automindtest.utils.Hooks;
import io.cucumber.java.pt.Dado;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;


import java.io.IOException;
import java.util.Properties;

public class OrangeHrmSteps extends SetUp {

    Global global = new Global();
    LoginPage loginPage = new LoginPage();
    HomePage homePage = new HomePage();
    protected WebDriver driver ;
    Properties prop = getProp();

    public OrangeHrmSteps() throws IOException {
    }

    @Dado("que acesso o site {string}")
    public void que_acesso_o_site(String string) {
        global.wait(2);
        homePage.trocaUrl(prop.getProperty("prop.website.url.orangehrm"));
        loginPage.loginPage("opensourcecms", "opensourcecms");
        global.wait(3);
        homePage.acessaMenuGeneralInformation();
//        global.wait(5);
    }
    @Before
    public void start_web() throws IOException {
        driver = SetUp.get_driver();
    }

    @After
    public void tearDown() {
        Hooks.tear_down();
		driver.quit();
    }
}
