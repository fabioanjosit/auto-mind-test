package automindtest.projeto.orangehrm.page;

import automindtest.Support.SetUp;
import automindtest.utils.Global;
import automindtest.utils.Global.Identify;
import org.openqa.selenium.WebElement;

public class HomePage extends SetUp {
	
	Global global = new Global();

	public void trocaUrl(String url){
		get_driver().navigate().to(url);
	}


	public void acessaMenuGeneralInformation(){
		WebElement menuAdmin =  global.getElementApplication(Identify.XPATH, "//span[normalize-space()='Admin']");
		menuAdmin.click();
		global.wait(2);
		global.logPrint("Mensagem Customizada clicando no Menu Admin", true);
//		global.moveMouseToElement(global.getElementApplication(Identify.XPATH, "//span[normalize-space()='Admin']"));
//		global.wait(3);
//		global.moveMouseToElement(global.getElementApplication(Identify.XPATH, "//span[normalize-space()='Organization']"));
//		global.wait(3);
//		global.moveMouseToElement(global.getElementApplication(Identify.XPATH, "//span[normalize-space()='General Information']"));
//		global.wait(3);
//		global.clickButton(Identify.XPATH, "//span[normalize-space()='General Information']");
		global.wait(2);
	}

	
}
