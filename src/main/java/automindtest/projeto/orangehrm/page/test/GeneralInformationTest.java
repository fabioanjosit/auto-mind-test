package automindtest.projeto.orangehrm.page.test;

import automindtest.Support.SetUp;
import automindtest.utils.Hooks;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import automindtest.projeto.orangehrm.page.HomePage;
import automindtest.projeto.orangehrm.page.LoginPage;

import java.io.IOException;
import automindtest.utils.Global;

public class GeneralInformationTest  extends SetUp{

	protected WebDriver driver ;
	
	Global global = new Global();
	LoginPage loginPage = new LoginPage();
	HomePage homePage = new HomePage();
	
	@Test
	public void loginPage(){
		global.wait(5);
		loginPage.loginPage("opensourcecms", "opensourcecms");
		global.wait(10);
		homePage.acessaMenuGeneralInformation();
		global.wait(5);
		
		
	}
	
	
	
	
	//========================================
	@Before
	public void start_web() throws IOException  { 
		driver = get_driver();
	}
	
	@After
	public void tearDown() {
		Hooks.tear_down();
//		driver.quit();
	}	
}
