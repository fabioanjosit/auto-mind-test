# language: pt
# encoding: utf-8

@Regressivo
Funcionalidade: Acessar o site Shoestock e realizar busca produtos e adicionar os produtos no carrinho,
  validando os produtos incluídos no carrinho na tela de pagamento.

  @Run
  Esquema do Cenário: Incluo produtos no carrinho
    Dado que acesso o site shoestock
    Quando procuro o produto "<produto1>"
    E adiciono o produto "<produto1>" com tamanho "<tamanho1>" no carrinho
    Quando valido o produto "<produto1>" no carrinho
    E procuro outro produto "<produto2>"
    E adiciono outro produto "<produto2>" com "<tamanho2>" no carrinho
    Então valido produtos "<produto1>" e "<produto2>" no carrinho na tela de pagamento
    Exemplos:
      | produto1                | produto2                        | tamanho1 | tamanho2 |
      | Slip On Couro Shoestock | Slip On Couro Shoestock Camurça | 43       | 43       |


