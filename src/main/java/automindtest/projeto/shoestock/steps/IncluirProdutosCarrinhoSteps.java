package automindtest.projeto.shoestock.steps;

import static org.junit.Assert.assertTrue;

import automindtest.projeto.shoestock.pages.HomePage;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class IncluirProdutosCarrinhoSteps {

	HomePage homePage = new HomePage();
	
	@Dado("que acesso o site shoestock")
	public void que_acesso_o_site_shoestock() throws Throwable {
		homePage.iniciaHomePage();
	}

	@Quando("procuro o produto {string}")
	public void procuro_o_produto(String arg1) throws Throwable {
		homePage.buscaProduto(arg1);
	}

	@Quando("^adiciono o produto \"([^\"]*)\" com tamanho \"([^\"]*)\" no carrinho$")
	public void adiciono_o_produto_com_tamanho_no_carrinho(String arg1, String arg2) throws Throwable {
		homePage.clicarItemProduto(arg1);
		homePage.clicarTamanho(arg2);
		homePage.clicarBotaoComprar();
	}

	@Quando("^valido o produto \"([^\"]*)\" no carrinho$")
	public void valido_o_produto_no_carrinho(String arg1) throws Throwable {
		assertTrue("O produto nao foi encontrado no carrinho", homePage.validaItensCarrinho(arg1));
	}

	@Quando("^procuro outro produto \"([^\"]*)\"$")
	public void procuro_outro_produto(String arg1) throws Throwable {
		homePage.clicarBotaoMaisProdutos();
		homePage.iniciaHomePage();
		homePage.buscaProduto(arg1);
	}

	@Quando("^adiciono outro produto \"([^\"]*)\" com \"([^\"]*)\" no carrinho$")
	public void adiciono_outro_produto_com_no_carrinho(String arg1, String arg2) throws Throwable {
		homePage.clicarItemProduto(arg1);
		homePage.clicarTamanho(arg2);
		homePage.clicarBotaoComprar();
	}

	@Então("^valido produtos \"([^\"]*)\" e \"([^\"]*)\" no carrinho na tela de pagamento$")
	public void valido_produtos_e_no_carrinho_na_tela_de_pagamento(String arg1, String arg2) throws Throwable {
		assertTrue("O produto nao foi encontrado no carrinho", homePage.validaItensCarrinho(arg1));
		assertTrue("O produto nao foi encontrado no carrinho", homePage.validaItensCarrinho(arg2));
	}
	
	
}
