package automindtest.projeto.shoestock.runners;

import automindtest.utils.ReportWeb;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(Cucumber.class)
@CucumberOptions(
	features = {"src/main/java/automindtest/projeto/shoestock/features"},
	glue = {"automindtest.projeto.shoestock.steps","automindtest.utils",
            "automindtest.projeto.shoestock.page"},
//	monochrome=true,
//	plugin = {"pretty", "html:target/report-html"},
	plugin = {"pretty", "json:target/cucumber-report/cucumber.json"},
//	snippets = SnippetType.UNDERSCORE,
//	strict = false
	tags = "@Run"
)

public class RunnerTest {
	@AfterClass
	public static void gerarReport(){
		try {
			ReportWeb.gerarReport("Sistema ShoeStock");
			Runtime.getRuntime().exec("cmd.exe /c mvn cluecumber-report:reporting");

		}catch (IOException e){
			e.printStackTrace();
		}
	}
}


