package automindtest.projeto.shoestock.pages;

import static automindtest.utils.Hooks.get_driver;

import java.io.IOException;

import automindtest.printsc.ScenarioRep;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import automindtest.utils.*;
import automindtest.Support.SetUp;
import automindtest.utils.Global.*;

public class HomePage extends SetUp{
	
	Global global = new Global();
	
	
	public void iniciaHomePage() throws IOException{
//		global.wait(8);
		if (global.elementExist(Identify.ID, "pnosp_div_close", 4)){
//			global.GeraPDF("Clico para fechar a propaganda");
			global.clickButton(Identify.ID,"pnosp_div_close");
		}
			global.logPrint("Acessando HomePage do site ShoeStock", true);
	}
	
	public void buscaProduto (String textoProduto) throws IOException {
		try{
			if(global.waitElementVisible(Identify.ID,"search-input" ,4)){
				global.clickButton(Identify.ID, "search-input");
				global.fillField(Identify.ID,"search-input",textoProduto);
//				global.GeraPDF("Digito o produto para buscar "+textoProduto);
				global.logPrint("Procurando o produto", true);
				global.clickButton(Identify.XPATH, "//button[@title='Buscar']");
			}
			global.wait(1);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clicarItemProduto(String textoItemProduto) throws IOException, InterruptedException{
		global.wait(1);
		if (global.waitElement("//a[@title='"+ textoItemProduto +"']/img")){
			global.wait(1);
//			global.GeraPDF("Clico no produto "+textoItemProduto);
			global.logPrint("Selecionando o Item", true);
			global.clickButton(Identify.XPATH,"//a[@title='"+ textoItemProduto +"']/img");
		}
	}
	
	public void clicarTamanho(String textoTamanho) throws IOException {
		global.wait(2);
		if(global.checkElement("(//a[@data-label='"+textoTamanho+"'])[1]")){
			global.wait(2);
//			global.GeraPDF("Clico no tamanho do produto " +textoTamanho);
			global.logPrint("Selecionando o Tamanho", true);
			global.clickButton(Identify.XPATH, "(//a[@data-label='"+textoTamanho+"'])[1]");
		}
	}
	
	public void clicarBotaoComprar() throws IOException {
		if (global.waitElementVisible(Identify.ID, "buy-button-now", 7)){
			global.wait(2);
//			global.GeraPDF("Clico no botão comprar");
			global.logPrint("Clicando no botão Comprar", true);
			global.clickButton(Identify.ID, "buy-button-now");
		}
	}
	
	public void clicarBotaoMaisProdutos(){
		if(global.checkElement("//a[normalize-space()='Escolher mais produtos']")){
			global.clickButton(Identify.XPATH, "//a[normalize-space()='Escolher mais produtos']");
		}
	}
	
	//========================VALIDACAO======================================//
	public boolean validaItensCarrinho(String textoItemProduto) throws IOException {
		global.wait(4);
//		global.GeraPDF("Valido item do produto no carrinho");
		global.logPrint("Validando itens no Carrinho", true);
		return global.checkElement("//h3[normalize-space()='"+ textoItemProduto+"']");
	}
	
	
}
