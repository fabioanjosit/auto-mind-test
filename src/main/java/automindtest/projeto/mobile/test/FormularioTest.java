package automindtest.projeto.mobile.test;

import automindtest.printsc.ScenarioRep;
import automindtest.utils.ReportWeb;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

public class FormularioTest {

    private AndroidDriver<MobileElement> driver;

    @Test
    public void TestMobileApp() {
        MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView");
        ScenarioRep.printScreen(driver,el1);
        el1.click();
        MobileElement el2 = (MobileElement) driver.findElementByAccessibilityId("nome");
        ScenarioRep.printScreen(driver,el2);
        el2.click();
        MobileElement el3 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout");
        ScenarioRep.printScreen(driver,el3);
        el3.click();
        MobileElement el4 = (MobileElement) driver.findElementByAccessibilityId("nome");
        el4.click();
        MobileElement el5 = (MobileElement) driver.findElementByAccessibilityId("nome");
        el5.click();
        el5.click();
        MobileElement el6 = (MobileElement) driver.findElementByAccessibilityId("console");
        el6.click();
        el6.click();
        MobileElement el7 = (MobileElement) driver.findElementByAccessibilityId("console");
        el7.click();
        MobileElement el8 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.Button[3]/android.widget.TextView");
        el8.click();
        MobileElement el9 = (MobileElement) driver.findElementByAccessibilityId("nome");
        el9.sendKeys("teste01");
        MobileElement el10 = (MobileElement) driver.findElementByAccessibilityId("console");
        el10.click();
        MobileElement el11 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[3]");
        el11.click();
        MobileElement el12 = (MobileElement) driver.findElementByAccessibilityId("slid");
        el12.click();
        MobileElement el13 = (MobileElement) driver.findElementByAccessibilityId("check");
        el13.click();
        MobileElement el14 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.Button[2]/android.widget.TextView");
        el14.click();

    }

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("deviceName", "emulator-5554");
        desiredCapabilities.setCapability("automationName", "uiautomator2");
//	    desiredCapabilities.setCapability("appPackage", "com.ctappium");
//	    desiredCapabilities.setCapability("appActivity", "com.ctappium.MainActivity");
        desiredCapabilities.setCapability(MobileCapabilityType.APP, "D:\\Projetos\\AutomacaoTest\\TestAndroid\\src\\main\\resources\\CTAppium_1_1.apk.apk");

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");

        driver = new AndroidDriver(remoteUrl, desiredCapabilities);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @AfterClass
    public static void gerarReport() {
        try {
            ReportWeb.gerarReport("Mobile - APK ");
            Runtime.getRuntime().exec("cmd.exe /c mvn cluecumber-report:reporting");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
