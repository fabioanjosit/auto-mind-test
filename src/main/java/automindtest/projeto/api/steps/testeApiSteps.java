package automindtest.projeto.api.steps;

import automindtest.projeto.api.endpoint.ReqbinApi;
import automindtest.utils.ApiTest;
import automindtest.Support.SetUp;
import automindtest.utils.Global;
import automindtest.utils.Hooks;
import com.google.gson.JsonParser;
import io.cucumber.java.pt.Dado;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class testeApiSteps extends SetUp {

    Global global = new Global();
    ReqbinApi reqbinApi = new ReqbinApi();
    protected WebDriver driver ;

    private String url = "https://api.trello.com/1/actions/592f11060f95a3d3d46a987a";

    @Dado("que acesso o API")
    public void que_acesso_o_site() {
        global.wait(5);
        ApiTest.setURL(url);
        Response resp  = ApiTest.reqGet();
        JSONObject jel = null;
//        jsonElement = new JsonParser().parse(resp.asString());

        String Result = resp.jsonPath().getMap("data.list").get("name").toString();
        System.out.println(Result);

        jel = new JSONObject(resp.asString());
//		JSONArray data = jel.getJSONArray("id");

//		String strCampo = data.getJSONObject(0).getString("id");
        String strCampo = (String) jel.get("id");
        System.out.println("strCampo: "+strCampo);
//        global.wait(5);
    }

    @Dado("que acesso a API reqbin.com url {string}")
    public void que_acesso_api_reqbin_com(String urlReqbin) {
        reqbinApi.setUrlReqbin(urlReqbin);
        reqbinApi.reqReqbinPost();
    }



}
