package automindtest.projeto.api.endpoint;

import automindtest.utils.ApiTest;

public class ReqbinApi {

    public void setUrlReqbin(String url){
        ApiTest.setURL(url);
    }
    public void reqReqbinPost(){
        ApiTest.reqPost(
                ApiTest.parseFileJson("json/reqbin_post.json")
                ,ApiTest.setHeader()
                ,null,
                false);

    }
}
