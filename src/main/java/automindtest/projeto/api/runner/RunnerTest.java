package automindtest.projeto.api.runner;

import automindtest.utils.ReportWeb;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(Cucumber.class)
@CucumberOptions(
	features = {"src/main/java/automindtest/projeto/api/feature"},
	glue = {"automindtest.projeto.api.steps","automindtest.utils"},
//	monochrome=true,
//	plugin = {"pretty", "html:target/report-html"},
	plugin = {"pretty", "json:target/report-project-test/cucumber.json"},
//	snippets = SnippetType.UNDERSCORE,
//	strict = false
	tags = "@API"
)

public class RunnerTest {
	@AfterClass
	public static void gerarReport(){
		try {
			ReportWeb.gerarReport("APIs");
			Runtime.getRuntime().exec("cmd.exe /c mvn cluecumber-report:reporting");

		}catch (IOException e){
			e.printStackTrace();
		}
	}
}


