package automindtest.printsc;

import com.google.common.base.Function;
import automindtest.printsc.ScreenShotException;

public class RunScreenShot {
    public <V, T> V take(Function<? super T, V> isTrue){
        try{
            return isTrue.apply(null);
        } catch (Exception e){
            throw new ScreenShotException( e.getMessage());
        }
    }
}
