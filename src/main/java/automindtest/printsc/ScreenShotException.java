package automindtest.printsc;

public class ScreenShotException extends RuntimeException{
    public ScreenShotException(String msg){ super(msg); }
    public ScreenShotException(Exception e ) { super(e);}
    public ScreenShotException(String msg, Exception e) { super(msg, e);}
}
