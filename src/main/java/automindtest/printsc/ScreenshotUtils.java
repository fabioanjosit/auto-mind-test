package automindtest.printsc;

import org.openqa.selenium.*;

public class ScreenshotUtils {
    private final static String JS_FOCUS =
            "arguments[%d].setAttribute('style', 'border: 5px solid red!important');arguments[%d].focus();arguments[%d].scrollIntoView(true);";
    private final static String JS_BACK =
            "arguments[%d].setAttribute('style', '%2$s');";

    public static byte[] screenshot(WebDriver webDriver) {
        return ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
    }

    public static Object executeJS(WebDriver webDriver, String javaScript, WebElement... webElements) {
        JavascriptExecutor jse = ((JavascriptExecutor) webDriver);
        return jse.executeScript(javaScript, (Object[]) webElements);
    }

    public static String getJSFocus(WebElement... webElements) {
        StringBuilder js = new StringBuilder();

        for (int x = 0; x < webElements.length; x++) {
            if (webElements[x].getText() != null)
                js.append(String.format(JS_FOCUS, x, x, x));
        }
        return js.toString();
    }

    public static String getJSBack(WebDriver webDriver, WebElement... webElements) {
        StringBuilder js = new StringBuilder();

        for (int x = 0; x < webElements.length; x++) {
            if (webElements.toString() != null)
                js.append(String.format(JS_BACK, x, webElements[x].getAttribute("style")));
        }
        return js.toString();
    }


}
