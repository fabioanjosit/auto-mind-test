package automindtest.printsc;

import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;

public class ScenarioRep {
    private static Map<Long, Scenario> repo = new HashMap<>();

    private ScenarioRep(){
    }

    public static void add(Scenario scenario){
        if (get() == null) repo.put(getId(), scenario);
    }

    public static void remove(){
        if (get() != null) repo.remove(getId());
    }

    public static Scenario get() { return repo.get(getId());}

    public static Long getId() { return Thread.currentThread().getId();}


    public static void printScreen(WebDriver driver){
        try{
            byte[] print = PrintScreen.take(driver);
                get().attach(print, "image/png", "print");
        }catch (Exception e){
            System.out.println("Erro ao tirar print só com driver "+e.getMessage());
        }
    }

    public static void printScreen(WebDriver driver, WebElement... webElements){
        try{
            byte[] print = PrintScreen.take(driver, webElements);
                get().attach(print, "image/png", "print");
        }catch (Exception e){
            System.out.println("Erro ao tirar print do elemento: "+e.getMessage());
        }
    }

    public static void insereMensagem(String text){
        if (text != null) get().log(text);
    }

}
