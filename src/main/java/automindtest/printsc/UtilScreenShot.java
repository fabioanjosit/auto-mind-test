package automindtest.printsc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

public class UtilScreenShot {
    public static IScreenshot<byte[]> screenShotWeb(final WebDriver webDriver){
        return new IScreenshot<byte[]>(){
            @Override
            public byte[] apply(Object input) { return ScreenshotUtils.screenshot(webDriver);}
        };
    }


    public static IScreenshot<byte[]> screenShotWeb(final WebDriver webDriver, final WebElement... webElements){
        return new IScreenshot<byte[]>() {
            @Override
            public byte[] apply(Object input){
                String jsFocus = ScreenshotUtils.getJSFocus(webElements);
                String jsBack = ScreenshotUtils.getJSBack(webDriver, webElements);
                ScreenshotUtils.executeJS(webDriver, jsFocus, webElements);
                byte[] screenshot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
                ScreenshotUtils.executeJS(webDriver, jsBack, webElements);
                return screenshot;
            }
        };
    }
}
