package automindtest.printsc;

import com.google.common.base.Function;

public interface IScreenshot<T> extends Function<Object, T> {
}
