package automindtest.printsc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PrintScreen {
    public static byte[] take(WebDriver webDriver) {
        return new RunScreenShot().take(UtilScreenShot.screenShotWeb(webDriver));
    }

    public static byte[] take(WebDriver webDriver, WebElement... webElements) {
        return new RunScreenShot().take(UtilScreenShot.screenShotWeb(webDriver, webElements));
    }

}
